from podcast.util import chunks


def test_chunks():
    assert list(chunks(range(2),1)) == [(0,1,[0]),(1,2,[1])]
    assert list(chunks(range(2),2)) == [(0,2,[0,1])]
    assert list(chunks(range(2),3)) == [(0,2,[0,1])]
    assert list(chunks(range(5),3)) == [(0,3,[0,1,2]), (3,5,[3,4])]
