"""
test the queue
"""
import json
import os
import sys

import httpretty
from httpretty import HTTPretty

from podcast import POST_ID_TYPE
from test import xml_headers, read_testdata

sys.path.append("../python")

from podcast.queue import QueueFactory, QueueData
from podcast.queue import Queue
from podcast.podcast import Podcast
from podcast.podpost import Podpost, PodpostFactory


def test_create_queue():
    """
    def if a queue is created
    """

    q1 = QueueFactory().get_queue()
    assert type(q1) == Queue

    q2 = QueueFactory().get_queue()
    assert q1 == q2


def test_queue_save():
    """
    does the queue save itself?
    """

    q = QueueFactory().get_queue()
    q.save()
    queueData : QueueData = QueueData.select().limit(1).get_or_none()
    assert queueData
    assert [] == json.loads(queueData.queue_as_json)



@httpretty.activate(allow_net_connect=False)
def test_insert_top():
    HTTPretty.register_uri(HTTPretty.GET, 'https://freakshow.fm/feed/opus/',
                           body=read_testdata('testdata/freakshow.rss'), adding_headers=xml_headers)
    q = QueueFactory().get_queue()
    l1 = len(q.podposts)

    Podpost.delete().execute()
    podcast, episodes = Podcast.create_from_url('https://freakshow.fm/feed/opus/')
    assert len(podcast.entry_ids_old_to_new) == 20
    e = podcast.get_entry(podcast.entry_ids_old_to_new[1])
    e2 = podcast.get_entry(podcast.entry_ids_old_to_new[0])

    PodpostFactory().persist(e)
    q.insert_top(e.id)
    assert q.podposts[0] == e.id
    assert QueueData.get_queue() == [e.id]

    PodpostFactory().persist(e2)
    q.insert_top(e2.id)

    assert q.podposts[0] == e2.id
    assert q.podposts[1] == e.id
    assert QueueData.get_queue() == [e2.id, e.id]

    q.save()


@httpretty.activate(allow_net_connect=False)
def test_insert_next():
    HTTPretty.register_uri(HTTPretty.GET, 'https://freakshow.fm/feed/opus/',
                           body=read_testdata('testdata/freakshow.rss'), adding_headers=xml_headers)
    q = QueueFactory().get_queue()
    l1 = len(q.podposts)

    Podpost.delete().execute()
    podcast, episodes = Podcast.create_from_url('https://freakshow.fm/feed/opus/')
    assert len(podcast.entry_ids_old_to_new) == 20
    e = podcast.get_entry(podcast.entry_ids_old_to_new[1])
    e2 = podcast.get_entry(podcast.entry_ids_old_to_new[0])

    PodpostFactory().persist(e)
    q.insert_next(e.id)
    assert q.podposts[0] == e.id
    assert QueueData.get_queue() == [e.id]

    PodpostFactory().persist(e2)
    q.insert_next(e2.id)

    assert q.podposts[1] == e2.id
    assert q.podposts[0] == e.id
    assert QueueData.get_queue() == [e.id, e2.id]

    q.save()
    assert QueueData.get_queue() == [e.id, e2.id]

@httpretty.activate(allow_net_connect=False)
def test_get_podposts():
    """
    Test listing of podposts
    """
    HTTPretty.register_uri(HTTPretty.GET, 'https://freakshow.fm/feed/opus/',
                           body=read_testdata('testdata/freakshow.rss'), adding_headers=xml_headers)
    HTTPretty.register_uri(HTTPretty.GET, 'http://freakshow.fm/podlove/file/5556/s/feed/c/opus/fs227-alien-wave.opus',
                           body="")

    queue = QueueFactory().get_queue()
    podcast, episodes = Podcast.create_from_url('https://freakshow.fm/feed/opus/')
    entry = podcast.entry_ids_old_to_new[1]
    entry2 = podcast.entry_ids_old_to_new[0]
    queue.insert_top(entry)
    queue.insert_top(str(entry2)) #test type sanitation
    postids = list(queue.get_podposts())
    assert len(postids) == 2
    for post in postids:
        assert type(post) == POST_ID_TYPE
        podpost = PodpostFactory().get_podpost(post)
        assert type(podpost) == Podpost
        assert post in [entry, entry2]
        assert queue.is_in_queue(podpost)
    list(queue.download(entry))
    refreshed_post = PodpostFactory().get_podpost(entry)
    assert os.path.exists(refreshed_post.file_path)
    assert refreshed_post.percentage == 100

@httpretty.activate(allow_net_connect=False)
def test_queue_favorites():
    """
    Test listing of podposts
    """

    HTTPretty.register_uri(HTTPretty.GET, 'https://freakshow.fm/feed/opus/',
                           body=read_testdata('testdata/freakshow.rss'), adding_headers=xml_headers)
    from podcast import favorite
    queue = QueueFactory().get_queue()
    assert len(queue.podposts) == 0
    url = 'https://freakshow.fm/feed/opus/'
    assert Podpost.select().count() == 0
    podcast, episodes = Podcast.create_from_url(url)
    assert len(podcast.entry_ids_old_to_new) == 20
    assert len(queue.podposts) == 0
    for post in (PodpostFactory().get_podpost(podcast.entry_ids_old_to_new[i]) for i in range(0, 10, 2)):
        post.favorite = True
        PodpostFactory().persist(post)
    assert len(queue.podposts) == 0
    for id in podcast.entry_ids_old_to_new:
        queue.insert_bottom(id)
    assert len(queue.podposts) == 20
    for id in podcast.entry_ids_old_to_new:
        queue.remove(id)
    assert len(queue.podposts) == 0
