import logging
import os
import tempfile

import pytest


logger = logging.getLogger(__name__)

@pytest.fixture(autouse=True, scope='session')
def dbsetup():
    init_db_and_storage_from_temp(tempfile.mkdtemp())


def init_db_and_storage_from_temp(temppath):
    os.environ["PODQAST_HOME"] = temppath
    logger.info("tempdir: %s", os.environ["PODQAST_HOME"])
    from podcast.constants import Constants
    from podcast.data_migration import setup_db
    if len(Constants._instances.items()) != 0:
        Constants().reload()
    else:
        Constants()
    setup_db()


@pytest.fixture(autouse=True, scope="function")
def cleanup():
    from podcast import constants
    logger.info("cleaning database %s",constants.db.database)
    cleanup_archive()
    cleanup_queue()
    cleanup_inbox()
    cleanup_podcast()
    cleanup_log()
    yield

def cleanup_log():
    from podcast.persistent_log import LogMessage
    LogMessage.delete().execute()


def cleanup_inbox():
    from podcast.inbox import InboxEntry
    InboxEntry.delete().execute()

def cleanup_archive():
    from podcast.archive import ArchiveFactory
    archive = ArchiveFactory().get_archive()
    for post in archive.get_podposts():
        archive.remove_podpost(post)


def cleanup_queue():
    from podcast.queue import QueueFactory
    queue = QueueFactory().get_queue()
    to_remove = list(queue.get_podposts())
    for post in to_remove:
        try:
            queue.remove(post)
        except:
            logging.exception("error during cleaning queue with post %s",post)
    queue.save()
    remaining = list(queue.get_podposts())
    if len(remaining) != 0:
        logging.error("Got remaining items %s %s ", [(r, r in to_remove) for r in remaining], to_remove)
    assert len(remaining) == 0


def cleanup_podcast():
    from podcast.podcast import Podcast, feedCache
    from podcast.podpost import Podpost, PodpostChapter
    Podcast.delete().execute()
    Podpost.delete().execute()
    PodpostChapter.delete().execute()
    feedCache.clear()