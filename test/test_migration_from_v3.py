import os
import tempfile
from distutils.dir_util import copy_tree

from podcast import data_migration, favorite
from podcast.archive import ArchiveFactory
from podcast.constants import db
from podcast.external import ExternalFactory
from podcast.factory import Factory
from podcast.inbox import InboxFactory
from podcast.podcastlist import PodcastListFactory
from podcast.podpost import Podpost, PodpostChapter
from podcast.queue import QueueFactory
from podcast.util import ilen
from test import conftest
from test.test_migration_from_v0 import resetSingletons, reset
import logging

logger = logging.getLogger(__name__)


def test_migration_v3():
    oldhome = os.environ["PODQAST_HOME"]
    try:
        with tempfile.TemporaryDirectory() as tmpdir:
            copy_tree(os.path.join(os.path.dirname(__file__), "testdata/migrationtests_v3/"), tmpdir)
            resetSingletons(tmpdir)
            data_migration.run_migrations(False)
            assert os.path.exists(os.path.join(tmpdir, "dataversion"))
            assert Factory().get_store().count() == 1
            podcasts = list(PodcastListFactory().get_podcast_list().get_podcasts())
            assert len(podcasts) == 1
            for podcast in PodcastListFactory().get_podcast_list().get_podcasts_objects():
                assert podcast != None
                assert podcast.count_episodes() > 0
                assert podcast.title
            hrefs = list(tup[0] for tup in Podpost.select(Podpost.href).tuples())
            assert len(hrefs) == 9
            assert len(hrefs) == len(set(hrefs))
            assert Podpost.select().count() == 9
            assert len(ArchiveFactory().get_archive().get_podposts()) == 2
            assert ilen(favorite.get_favorites()) == 1
            assert ilen(QueueFactory().get_queue().get_podposts()) == 3
            assert ilen(ExternalFactory().get_external().get_podposts()) == 0
            assert ilen(InboxFactory().get_inbox().get_podposts()) == 0
            assert PodpostChapter.select().count() == 0
            assert 2431451000 == QueueFactory().get_queue()._get_top().duration
    finally:
        logger.info("closing db")
        db.close()
        reset(oldhome)
