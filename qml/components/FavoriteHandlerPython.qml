import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: favoritehandler

    signal favoriteToggled(var podpost, bool favstate)
    signal favoriteListData(int offset, var data)

    Component.onCompleted: {
        setHandler("favoriteToggled", favoriteToggled)
        setHandler("favoriteListData", favoriteListData)

        addImportPath(Qt.resolvedUrl('.'));
        importModule('FavoriteHandler', function () {
            console.log('FavoriteHandler is now imported')
        })
    }

    function toggleFavorite(podpost) {
        call("FavoriteHandler.toggle_favorite", [podpost, doFavDownConf.value], function() {});
    }
    function getFavorites(podurl) {
        if (podurl === "home") {
            call("FavoriteHandler.favoritehandler.getfavorites", function() {});
        } else {
            call("FavoriteHandler.favoritehandler.getfavorites", [podurl], function() {});
        }

    }
}
