import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: fyydhandler

    signal podSearch(var podcasts)
    signal podcastList(var pclist)

    Component.onCompleted: {
        setHandler("podsearch", podSearch)
        setHandler("podcastlist", podcastList)

        addImportPath(Qt.resolvedUrl('.'));
        importModule('FyydDe', function () {
            console.log('FyydDe is now imported')
        })
    }

    function doSearch(query) {
        call("FyydDe.fyydde.searchpods", [query], function() {});
    }
}
