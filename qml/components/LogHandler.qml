import QtQuick 2.0
import io.thp.pyotherside 1.4

Python {
    id: loghandler

    signal logData(int offset, var data)

    Component.onCompleted: {
        setHandler("logData", logData)
        addImportPath(Qt.resolvedUrl("/usr/share/harbour-podqast/python"))
        addImportPath(Qt.resolvedUrl('.'))
        importModule('LogHandler', function () {
            console.log('LogHandler is now imported')
        })
    }

    function get_logs() {
        call("LogHandler.get_logs", function () {})
    }
}
