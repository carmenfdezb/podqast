import QtQuick 2.0
import Sailfish.Silica 1.0

Column {
    id: backupb
    property bool backupRunning: false
    property bool opmlWriteRunning: false
    property bool opmlButtonVisible: true
    property bool doBackupStore: false

    spacing: Theme.paddingMedium

    Connections {
        target: feedparserhandler
        onBackupDone: {
            console.log("Notify backup done")
            appNotification.previewSummary = qsTr("Backup done")
            appNotification.previewBody = qsTr("Backup done to ") + tarpath
            appNotification.body = qsTr("Backup done to ") + tarpath
            appNotification.publish()
            backupRunning = false
        }

        onOpmlSaveDone: {
            console.log("Notify opml save done")
            appNotification.previewSummary = qsTr("OPML file saved")
            appNotification.previewBody = qsTr("OPML file saved to ") + opmlpath
            appNotification.body = qsTr("OPML file saved to ") + opmlpath
            appNotification.publish()
            opmlWriteRunning = false
        }
    }

    Button {
        id: importbutton
        text: qsTr("Backup")
        enabled: !backupRunning
        width: parent.width
        onClicked: {
            backupRunning = true
            feedparserhandler.doBackup(doBackupStore)
        }
        BusyIndicator {
            size: BusyIndicatorSize.Medium
            anchors.centerIn: importbutton
            running: backupRunning
        }
    }

    Button {
        id: opmlbutton
        text: qsTr("Save to OPML")
        enabled: !opmlWriteRunning
        width: parent.width
        visible: opmlButtonVisible
        onClicked: {
            opmlWriteRunning = true
            feedparserhandler.doWriteOpml()
        }

        BusyIndicator {
            size: BusyIndicatorSize.Medium
            anchors.centerIn: parent
            running: opmlWriteRunning
        }
    }
}
