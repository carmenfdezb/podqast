# -*- coding: utf-8 -*-
import asyncio

import pyotherside

from podcast.podpost import PodpostFactory
from podcast.queue import QueueFactory
from podcast.archive import ArchiveFactory
import logging

from AsyncWrapper import AsyncWrapper, async_threaded

logger = logging.getLogger(__name__)


def reset_post_position(id):
    podpost = PodpostFactory().get_podpost(id)
    if not podpost:
        raise Exception("Could not find post %d",id)
    podpost.reset_position_if_almost_finished()
    return podpost


class QueueHandler(object):

    async def get_queue_posts(self, moved_post_id=""):
        """
        Return a list of all queue posts
        """

        entries = []

        for entry in QueueFactory().get_podposts_objects():
            edata = entry.get_data()
            edata["moved"] = edata["id"] == moved_post_id
            entries.append(edata)
            if len(entries) == 1:
                if edata["haschapters"]:
                    chapters = entry.get_chapters()
                else:
                    chapters = []
                pyotherside.send("setFirst", entries[0], chapters)
        logger.info("queue list with %d entries", len(entries))
        pyotherside.send("createList", entries, moved_post_id)

    async def get_first_entry(self):
        """
        Return the data of the first entry
        """

        post = QueueFactory().get_first_podpost_object()
        if post is not None:
            pyotherside.send(
                "firstentry", post.get_data()
            )

    async def queue_insert_top(self, id, doplay=False):
        """
        Insert Podpost element to top of queue
        """
        post = reset_post_position(id)
        queue = QueueFactory().get_queue()
        if queue.insert_top(id):
            pyotherside.send("posplay", post.get_position)
            logger.info("inserted %d as first post in queue", id)
            await self.get_queue_posts()

    async def queue_insert_next(self, id, doplay=False):
        """
        Insert Podpost at next Entry in Queue
        """

        reset_post_position(id)
        queue = QueueFactory().get_queue()
        queue.insert_next(id)
        reset_post_position(id)
        await self.get_queue_posts(moved_post_id=id)

    async def queue_insert_bottom(self, id, doplay=False):
        """
        Insert Podpost at last position in queue
        """
        reset_post_position(id)
        queue = QueueFactory().get_queue()
        queue.insert_bottom(id)
        await self.get_queue_posts(moved_post_id=id)

    async def queue_move_up(self, id):
        """
        move element up
        """

        if QueueFactory().get_queue().move_up(id) == 0:
            await self.send_first_episode_chapters(id)
            await self.queue_play(only_start_if_playing=True)
        await self.get_queue_posts(moved_post_id=id)
        QueueFactory().get_queue().save()

    async def queue_move_down(self, id):
        """
        move element down
        """

        QueueFactory().get_queue().move_down(id)
        await self.get_queue_posts(moved_post_id=id)
        QueueFactory().get_queue().save()

    async def queue_play(self, only_start_if_playing = False):
        """
        Get the play information from queue
        """

        queue = QueueFactory().get_queue()
        data = queue.play()
        logger.info("Playing")

        if not data["url"]:
            pyotherside.send("audioNotExist")
        else:
            pyotherside.send("playing", data["url"], data["position"],only_start_if_playing)

    async def queue_pause(self, position):
        """
        Set Pause
        """

        queue = QueueFactory().get_queue()

        queue.pause(position)

        pyotherside.send("pausing")

    async def queue_stop(self, position):
        """
        Stop and save position
        """

        queue = QueueFactory().get_queue()
        queue.stop(position)
        queue.save()
        pyotherside.send("stopping")

    async def queue_seek(self, position):
        """
        Set queue position
        """

        queue = QueueFactory().get_queue()
        queue.position(position)
        queue.save()
        pyotherside.send("positioning")

    async def queue_to_archive(self, id):
        """
        Move an item to archive
        """

        queue = QueueFactory().get_queue()
        archive = ArchiveFactory().get_archive()
        if queue.get_top_id() == id:
            pyotherside.send("stopping")
        queue.remove(id)
        archive.insert(id)

        await self.get_queue_posts()

    async def queue_top_to_archive(self, playnext):
        """
        Remove top Element from queue
        """

        queue = QueueFactory().get_queue()
        top_id = queue.get_top_id()
        top = PodpostFactory().get_podpost(top_id)
        top.listened = True
        PodpostFactory().persist(top)
        await self.queue_to_archive(top_id)
        if playnext:
            await self.queue_play()

    @async_threaded()
    async def queue_download(self, id):
        """
        Do the Download of an archive
        """

        queue = QueueFactory().get_queue()
        for perc in queue.download(id):
            pyotherside.send("downloading", id, perc)

    @async_threaded()
    async def queue_download_all(self):
        """
        Download the whole queue if not done
        """
        queue = QueueFactory().get_queue()
        queue.download_all()

    async def update_position(self, post_id, position):
        podpost = PodpostFactory().get_podpost(post_id)
        if podpost:
            logger.info("setting position of podpost %s %s to %s", podpost.id, podpost.title, position)
            podpost.update_position(position=position)

    async def set_duration(self, duration):
        """
        Set podposts duration if not set
        """

        QueueFactory().get_queue().set_duration(duration)
        await self.get_queue_posts()

    async def get_episode_chapters(self, id):
        """
        Sends the list of chapters for an episode
        id: id of the episode
        """

        entry = PodpostFactory().get_podpost(id)
        chapters = entry.get_chapters()
        pyotherside.send("episodeChapters", chapters)

    async def send_first_episode_chapters(self, id):
        """
        Sends episode data and chapter list
        (necessary when chapters of the currently
        playing episode are selected/deselected)
        """
        entry = PodpostFactory().get_podpost(id)
        edata = entry.get_data()
        chapters = entry.get_chapters()
        pyotherside.send("setFirst", edata, chapters)

    async def toggle_chapter(self, episodeid, chapterid):
        """
        Toggles the selected state of a chapterid
        episodeid: id of the episode
        chapterid: chapter number
        """
        entry = PodpostFactory().get_podpost(episodeid)
        entry.toggle_chapter(chapterid)

    def doexit(self):
        """
        On exit: we need to stop ourself
        """
        asyncio.run(self.queue_stop(-1))


instance = AsyncWrapper(QueueHandler())
