import sys
sys.path.append("/usr/share/harbour-podqast/python")

from podcast import data_migration

def needs_migration():
    return data_migration.needs_migration()

def run_migrations(strict):
    data_migration.run_migrations(strict)