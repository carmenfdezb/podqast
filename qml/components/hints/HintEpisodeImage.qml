import QtQuick 2.0
import Sailfish.Silica 1.0
import "../"

EpisodeImage {
    property int baseindex: 2
    property int relativeIndex: index - baseindex
    showDownladingState: false
    showIsExternalAudio: relativeIndex === 3
    showIsPlaying: relativeIndex === 1
    showIsListened: relativeIndex === 2
    percentage: relativeIndex === 0 ? 100 : 0
    anchors.fill: centeredComponent
    property string logo_url: "../../images/podcast.png"
    width: Theme.iconSizeExtraLarge
    height: Theme.iconSizeExtraLarge
}
