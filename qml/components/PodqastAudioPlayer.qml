import QtQuick 2.0
import io.thp.pyotherside 1.4
import QtMultimedia 5.0

MediaPlayer {
    id: mediaplayer

    readonly property bool isPlaying: playbackState === Audio.PlayingState

    playbackRate: globalPlayrateConf.value
    autoLoad: true
    autoPlay: autoPlayNextInQueue.value

    onSourceChanged: console.log(
                         "changing mediaplayer src to: " + mediaplayer.source)

    onPlaybackRateChanged: {
        if (isPlaying)
            seek(position - 0.01)
    }

    onPaused: {
        console.log("onPaused status: " + mediaplayer.status)
        if (mediaplayer.status == 6) {
            playerHandler.pause()
        }
    }
    onSeekableChanged: {
        console.log("onSeekableChanged status: " + mediaplayer.status + " seekPos: "
                    + playerHandler.seekPos + " dostartafterSeek:" + playerHandler.doStartAfterSeek)
        playerHandler.setDuration()
        if (playerHandler.seekPos > 0) {
            mediaplayer.seek(playerHandler.seekPos)
            playerHandler.seekPos = 0.0
        }
        playerHandler.getaktchapter()
        if (playerHandler.doStartAfterSeek) {
            mediaplayer.play()
            playerHandler.doStartAfterSeek = false
        }
    }
    onStatusChanged: {
        switch (mediaplayer.status) {
        case MediaPlayer.EndOfMedia:
            console.log("End of media " + mediaplayer.source)
            if (mediaplayer.duration === -1) {
                console.log("mediaplayer does not even know a duration, do nothing!")
                break
            }
            queuehandler.queueTopToArchive(autoPlayNextInQueue.value)
            break
        case MediaPlayer.StoppedState:
            console.log("Stopped Mediaplayer " + mediaplayer.source)
            break
        case MediaPlayer.PlayingState:
            console.log("Playing " + mediaplayer.source)
            break
        case MediaPlayer.PausedState:
            console.log("Paused " + mediaplayer.source)
        }
    }
}
