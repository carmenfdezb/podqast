<p>
<h3>Authors</h3>
  <a href="https://talk.maemo.org/member.php?u=33544">Cy8aer</a> (coding)<br>
  <a href="https://gitlab.com/thigg">Thigg</a> (coding)<br>
  <a href="https://gitlab.com/lukasj/">Lukas J</a> (fyyd.de)<br>
  <a href="https://talk.maemo.org/member.php?u=31168">BluesLee</a> (testing)<br>
  <a href="http://www.einbilder.de">Daniel Noll</a> (painting)<br>
  <a href="https://gitlab.com/flesser">Florian Eßer</a> (some help on tumblr)
<h4>Translations</h4>
  <a href="https://gitlab.com/eson">Åke Engelbrektson</a> (sv)<br>
  <a href="https://gitlab.com/carmenfdezb">Carmen F. B.</a> (es)<br>
  <a href="https://github.com/lturpinat/">Louis Turpinat</a> (fr)
</p>

<p>
<h3><a href="https://gitlab.com/cy8aer/podqast">PodQast</a> uses</h3>
<a href="https://github.com/kurtmckee/feedparser">feedparser</a>
by Kurt McKee and Mark Pilgrim<br>
<a href="http://gpodder.org/mygpoclient/">mygpoclient</a> by Thomas Perl and others<br>
<a href="https://github.com/Alir3z4/html2text/">html2text</a> by Aaron Schwartz
and others<br>
<a href="https://github.com/quodlibet/mutagen">mutagen</a> by Christoph Reiter and Joe Wreschnig<br>
<a href="https://github.com/coleifer/peewee/">peewee</a> by Charles Leifer<br>
<a href="https://github.com/Michal-Szczepaniak/Musikilo">TabHeader</a> by Michał Szczepaniak<br>

</p>

<p>
<h3>Privacy</h3>
PodQast stores all information on the device. For quering
  information of podcasts it uses
  the <a href="https://gpodder.net">gpodder.net</a> service. Every
  subscribed podcast service may have it's own user data
  handling. Please inform yourself at these services themself.
</p>

<p>
<small>Version 2.71</small>
</p>

<p>Copyright (c) 2018 Thomas Renard</p>
