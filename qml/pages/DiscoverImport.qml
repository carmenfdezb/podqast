import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    allowedOrientations: Orientation.All
    property int margin: Theme.paddingMedium

    SilicaFlickable {
        id: mainflick
        anchors.fill: parent

        property bool downloadGpodder: false
        property bool downloadOpml: false

        Connections {
            target: feedparserhandler
            onOpmlImported: {
                console.log("Notify the OPML imported")
                appNotification.previewSummary = opmlcount + qsTr(
                            " Podcasts imported")
                appNotification.previewBody = opmlcount + qsTr(
                            " Podcasts imported from OPML")
                appNotification.body = opmlcount + qsTr(
                            " Podcasts imported from OPML")
                appNotification.publish()
                mainflick.downloadGpodder = false
                mainflick.downloadOpml = false
            }
        }

        PrefAboutMenu {}

        Row {
            id: discovertitle
            width: page.width
            Column {
                id: column
                width: page.width
                spacing: Theme.paddingLarge

                PageHeader {
                    title: qsTr("Discover by Importing")
                }
            }
        }
        Column {
            id: texturlcolumn
            anchors.top: discovertitle.bottom
            width: parent.width
            height: Theme.paddinglarge
            TextField {
                id: opmlfile
                width: parent.width
                focus: false
                placeholderText: qsTr('Pick an OPML file')
                label: qsTr('OPML file')
                onClicked: pageStack.push(filepickerpage)
            }
            Component {
                id: filepickerpage
                FilePickerPage {
                    nameFilters: ['*.opml']
                    onSelectedContentPropertiesChanged: {
                        opmlfile.text = selectedContentProperties.filePath
                        opmlfile.focus = false
                    }
                }
            }
        }
        Column {
            id: importb
            height: Theme.itemSizeLarge
            anchors.top: texturlcolumn.bottom
            Button {
                id: importbutton
                text: qsTr("Import OPML")
                onClicked: {
                    Remorse.popupAction(page, qsTr("Import OPML File"),
                                        function () {
                                            mainflick.downloadOpml = true
                                            feedparserhandler.importOpml(
                                                        opmlfile.text)
                                        })
                }
                BusyIndicator {
                    size: BusyIndicatorSize.Medium
                    anchors.centerIn: parent
                    running: mainflick.downloadOpml
                }
            }
        }

        Column {
            id: gpodderimport
            height: Theme.itemSizeExtraLarge
            anchors.top: importb.bottom
            Button {
                text: qsTr("Import from Gpodder")
                onClicked: {
                    Remorse.popupAction(page, qsTr("Import Gpodder Database"),
                                        function () {
                                            mainflick.downloadGpodder = true
                                            feedparserhandler.importGpodder()
                                        })
                }
                BusyIndicator {
                    size: BusyIndicatorSize.Medium
                    anchors.centerIn: parent
                    running: mainflick.downloadGpodder
                }
            }
        }
        Column {
            id: warningcolumn
            anchors.top: gpodderimport.bottom
            height: Theme.itemSizeExtraSmall
            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: margin
                }

                text: qsTr("Please note: Importing does take some time!")
                font.pixelSize: Theme.fontSizeSmall
                height: Theme.itemSizeSmall
            }
        }

        PlayDockedPanel {}
    }
}
