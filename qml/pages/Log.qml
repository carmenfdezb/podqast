import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    Connections {
        target: loghandler
        onLogData: {
            if (offset == 0)
                logModel.clear()
            for (var i = 0; i < data.length; i++) {
                //console.log(JSON.stringify(data[i]))
                logModel.append(data[i])
            }
            console.log("count:" + logModel.count)
        }
    }

    SilicaListView {
        width: parent.width
        anchors {
            top: parent.top
            bottom: parent.bottom
        }
        header: PageHeader {
            title: qsTr("Logs")
        }

        Component.onCompleted: loghandler.get_logs()

        model: ListModel {
            id: logModel
        }

        delegate: Column {
            clip: true
            x: Theme.horizontalPageMargin
            width: parent.width - 2 * x
            Label {
                wrapMode: Text.WordWrap
                width: parent.width
                //: one of the LogTypes in persistent_log.py
                text: qsTr(messagetype)
                color: Theme.highlightColor
            }
            Label {
                font.pixelSize: Theme.fontSizeTiny
                text: new Date(insert_date).toLocaleString(Qt.locale())
                color: Theme.secondaryHighlightColor
                width: parent.width
            }

            Label {
                elide: Text.ElideRight
                wrapMode: Text.WordWrap
                width: parent.width
                text: create_description(model.messagetype, model.params)
                font.pixelSize: Theme.fontSizeTiny
            }
        }

        ViewPlaceholder {
            enabled: logModel.count === 0
            text: qsTr("No logs")
        }

        VerticalScrollDecorator {}
    }

    function create_description(type, paramstring) {
        var args = JSON.parse(paramstring)
        console.debug("formatting " + JSON.stringify(args))
        switch (type) {
        case "Refresh304":
            return qsTr("Skipped refresh on '%1', because the server responded with 'Not Modified'").arg(
                        args["title"])
        case "SuccessfulRefresh":
            return qsTr("Updated '%1'. %2 new episodes").arg(args["title"]).arg(
                        args["num_new_episodes"])
        case "FeedParseError":
            return qsTr("'%1' could not be refreshed because: %2").arg(
                        args["title"]).arg(args["errormsg"])
        default:
            return paramstring
        }
    }
}
