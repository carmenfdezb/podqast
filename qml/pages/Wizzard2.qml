import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0

Dialog {
    id: page
    property string abouttext
    property int margin: 30

    function accept() {
        if(canAccept) {
            _dialogDone(DialogResult.Accepted)
        }
        doWizzardConf.value = false
        pageStack.clear()
        pageStack.replace(Qt.resolvedUrl("Discover.qml"), {}, PageStackAction.Immediate)
    }

    onAccepted: {
        accept()
    }

    SilicaFlickable {
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: wizz.height + wizz.spacing + mainflick.height + mainflick.spacing
        property int refreshTime

        VerticalScrollDecorator { }

        Column {
            id: wizz
            width: parent.width

            DialogHeader {
                acceptText: qsTr("Let's start...")
                cancelText: qsTr("Back to info")
            }
        }

        SilicaFlickable {
            id: mainflick
            width: parent.width
            anchors.top: wizz.bottom
            // contentHeight: page.height

            property bool downloadGpodder: false
            property bool downloadOpml: false

            Connections {
                target: feedparserhandler
                onOpmlImported: {
                    console.log("Notify the OPML imported")
                    appNotification.previewSummary = opmlcount + qsTr(" Podcasts imported")
                    appNotification.previewBody = opmlcount + qsTr(" Podcasts imported from OPML")
                    appNotification.body = opmlcount + qsTr(" Podcasts imported from OPML")
                    appNotification.publish()
                    mainflick.downloadGpodder = false
                    mainflick.downloadOpml = false
                }
            }
            Column {
                id: describecol
                width: parent.width
                height: Theme.itemSizeLarge
                spacing: margin
                Label {
                    anchors {
                        left: parent.left
                        right: parent.right
                        margins: margin
                    }
                    text: qsTr("You are now able to import old stuff. You can import from Discover lateron. Please note: Importing does take some time!")
                    font.pixelSize: Theme.fontSizeSmall
                    wrapMode: Text.WordWrap
                }
            }

//            Column {
//                id: opmllabel
//                spacing: 10
//                anchors.top: describecol.bottom
//                width: parent.width
//                height: Theme.itemSizeExtraSmall

//                Label {
//                    text: qsTr("OPML File")
//                    color: Theme.highlightColor
//                }
//            }

            Column {
                id: texturlcolumn
                anchors.top: describecol.bottom
                width: parent.width
                height: Theme.itemSizeMedium
                spacing: margin
                TextField {
                    id: opmlfile
                    width: parent.width
                    focus: false
                    placeholderText: qsTr('Pick an OPML file')
                    label: qsTr('OPML file')
                    onClicked: pageStack.push(filepickerpage)
                }
                Component {
                    id: filepickerpage
                    FilePickerPage {
                        nameFilters: ['*.opml']
                        onSelectedContentPropertiesChanged: {
                            opmlfile.text = selectedContentProperties.filePath
                            opmlfile.focus = false
                        }
                    }
                }
            }
            Column {
                id: importb
                height: Theme.itemSizeLarge
                anchors.top: texturlcolumn.bottom
                spacing: margin
                Button {
                    id: importbutton
                    anchors.leftMargin: Theme.paddingMedium
                    text: qsTr("Import OPML")
                    onClicked: {
                        Remorse.popupAction(page, qsTr("Import OPML File"), function() {
                            mainflick.downloadOpml = true
                            feedparserhandler.importOpml(opmlfile.text)
                        })
                    }
                    BusyIndicator {
                        size: BusyIndicatorSize.Medium
                        anchors.centerIn: parent
                        running: mainflick.downloadOpml
                    }
                }
            }

            Column {
                id: gpodderimport
                height: Theme.itemSizeExtraLarge
                anchors.top: importb.bottom
                spacing: margin
                Button {
                    text: qsTr("Import from Gpodder")
                    anchors.leftMargin: Theme.paddingMedium
                    onClicked: {
                        Remorse.popupAction(page, qsTr("Import Gpodder Database"), function() {
                            mainflick.downloadGpodder = true
                            feedparserhandler.importGpodder()
                        })
                    }
                    BusyIndicator {
                        size: BusyIndicatorSize.Medium
                        anchors.centerIn: parent
                        running: mainflick.downloadGpodder
                    }
                }
            }

            Column {
                id: whatnext
                anchors.top: gpodderimport.bottom
                width: parent.width
                height: Theme.itemSizeMedium
                spacing: margin
                Label {
                    anchors {
                        left: parent.left
                        right: parent.right
                        margins: margin
                    }
                    height: parent.height
                    text: qsTr("When you now start you can discover new podcasts. After that you can for example select new posts to play from the Library.")
                    font.pixelSize: Theme.fontSizeSmall
                    wrapMode: Text.WordWrap
                }
            }
        }
    }
}
