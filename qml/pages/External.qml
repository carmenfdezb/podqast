import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Component.onCompleted: {
        console.log("rendering external entries")
        externalhandler.getExternalEntries()
    }

    Connections {
        target: externalhandler
        ignoreUnknownSignals: true
        onCreateExternalList: {
            console.log("onCreateExternalList")
            externalPostModel.clear()
            for (var i = 0; i < data.length; i++) {
                externalPostModel.append(data[i])
            }
        }
        onExternalUpdated: {
            externalhandler.getExternalEntries()
        }
    }

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        VerticalScrollDecorator {}
        PrefAboutMenu {}


        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        Column {
            id: externaltitle

            width: page.width

            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("External Audio")
            }
        }

        SilicaListView {
            id: archivepostlist
            clip: true
            anchors.top: externaltitle.bottom
            width: parent.width
            height: page.height - pdp.height - externaltitle.height
            section.property: 'esection'
            section.delegate: SectionHeader {
                text: section
                horizontalAlignment: Text.AlignRight
            }
            ViewPlaceholder {
                enabled: externalPostModel.count == 0
                text: qsTr("Rendering")
                hintText: qsTr("Collecting Posts")
                verticalOffset: -externaltitle.height
            }

            model: ListModel {
                id: externalPostModel
            }
            delegate: ArchivePostListItem {}
        }

        PlayDockedPanel {
            id: pdp
        }
    }
}
