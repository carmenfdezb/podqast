"""
A podcast queue. This does not need to be a singleton. Queue manages the playing queue
It instanciates Podposts via Factory if needed.
"""
import functools
import json
import sys
import os
import threading
from typing import Iterator, List, Optional

import pyotherside

from peewee import TextField, AutoField
from podcast import POST_ID_TYPE
from podcast.constants import BaseModel
from podcast.persistent_log import persist_log, LogType
from podcast.podpost import PodpostFactory, Podpost

sys.path.append("../")

from podcast.singleton import Singleton
import logging

logger = logging.getLogger(__name__)
queuename = "the_queue"

# safeguard to prevent parallel execution
queue_db_lock = threading.Lock()
class QueueData(BaseModel):

    id: AutoField = AutoField(primary_key=True)
    queue_as_json: str = TextField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @classmethod
    def get_queue(cls) -> List[POST_ID_TYPE]:
        with queue_db_lock:
            data: Optional[QueueData] = QueueData.select().limit(1).get_or_none()
            if data:
                return json.loads(data.queue_as_json)
            else:
                return []

    @classmethod
    def save_queue(cls, queue: List[POST_ID_TYPE]):
        with queue_db_lock:
            from_db: QueueData = QueueData.select().limit(1).get_or_none()
            if not from_db:
                from_db = QueueData()
            queue_as_json: str = json.dumps(queue)
            logger.info("saving queue %s", queue_as_json)
            from_db.queue_as_json = queue_as_json
            from_db.save()




def sanitize_podpost(func):
    """
    convert string ids to int
    @param func:
    @return:
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        largs : List = list(e for e in args)
        podpost = largs[1]
        if type(podpost) != POST_ID_TYPE:
            largs[1] = POST_ID_TYPE(podpost)
        return func(largs[0], largs[1], **kwargs)

    return wrapper


class Queue:
    """
    The podcast Queue. It has a list of podposts
    """
    podposts: List[POST_ID_TYPE]

    def __init__(self, queue: List[POST_ID_TYPE] = None):
        """
        Initialization
        @param queue:
        """
        self.podposts = queue if queue else [] # Id list of podposts

    def _get_ele_pos(self, index) -> int:
        """
        get the position of an element
        """

        pos = 0
        for i in self.podposts:
            if i == index:
                return pos
            pos += 1
        return -1

    def save(self):
        """
        pickle this element
        """

        QueueData.save_queue(self.podposts)

    def _get_top(self) -> Podpost:
        """
        get the top podpost
        """

        if self.podposts == []:
            self.top = None
        else:
            podpost = self.podposts[0]
            self.top = PodpostFactory().get_podpost(podpost)

        return self.top

    @sanitize_podpost
    def insert_top(self, podpost) -> bool:
        """
        Insert the podpost at top of queue. The post then is triggered to download
        its audio file.

        podpost: the podpost to be inserted. The post then is trigge
        return: False - was already the first post, otherwise True
        """

        logger.info("queue insert top %d, current length: %d", podpost, len(self.podposts))
        if self.podposts == []:
            self.podposts.append(podpost)
        else:
            if self.podposts[0] == podpost:
                return False
            pos = self._get_ele_pos(podpost)
            if pos >= 0:
                self.podposts.remove(podpost)
            self.podposts.insert(0, podpost)

        self.save()
        pyotherside.send("needDownload", podpost)
        return True

    @sanitize_podpost
    def insert_next(self, podpost):
        """
        put podpost to the second position
        """

        if type(podpost) != POST_ID_TYPE:
            POST_ID_TYPE(podpost)

        if len(self.podposts) <= 1:
            self.podposts.append(podpost)
        else:
            pos = self._get_ele_pos(podpost)
            if pos >= 0:
                self.podposts.remove(podpost)
            self.podposts.insert(1, podpost)

        self.save()
        pyotherside.send("needDownload", podpost)

    @sanitize_podpost
    def insert_bottom(self, podpost):
        """
        append a podpost at the end of the queue

        podpost: podpost to be appended
        """
        pos = self._get_ele_pos(podpost)
        if pos >= 0:
            self.podposts.remove(podpost)
        self.podposts.append(podpost)

        self.save()
        pyotherside.send("needDownload", podpost)


    @sanitize_podpost
    def remove(self, podpost):
        """
        remove podpost from list
        """

        if podpost in self.podposts:
            self.podposts.remove(podpost)
            post = PodpostFactory().get_podpost(podpost)
            post.delete_file()
            post.state = 0
            PodpostFactory().persist(post)
            if len(self.podposts) > 0 and podpost == self.podposts[0] and len(self.podposts) > 1:
                post1 = PodpostFactory().get_podpost(self.podposts[1])
                pyotherside.send("setpos", post1.position)
        else:
            PodpostFactory().persist(PodpostFactory().get_podpost(podpost))

        self.save()

    @sanitize_podpost
    def download(self, podpost: POST_ID_TYPE) -> Iterator[float]:
        """
        let a podpost download
        @return an iterator over the download progress. You need to iterate this iterator to complete the download
        """

        if podpost in self.podposts:
            post = PodpostFactory().get_podpost(podpost)
            for perc in post.download():
                yield perc
            if podpost == self.podposts[0] and post.file_path:
                pyotherside.send("firstDownloaded", post.file_path, post.logo_path)

    def download_all(self):
        """
        Download all posts in queue
        """

        for podpost in self.podposts:
            post = PodpostFactory().get_podpost(podpost)
            if post is None:
                continue
            try:
                if post.file_path:
                    if os.path.isfile(post.file_path):
                        continue
            except:
                pass
            logger.debug("%s: %s needs download", podpost, post.title)
            pyotherside.send("needDownload", podpost)

    def update_position(self, position):
        """
        get the human readable time of podpost
        """

        PodpostFactory().get_podpost(self.podposts[0]).update_position(position=position)

    @sanitize_podpost
    def move(self, podpost, pos):
        """
        Move a podpost to another position (drag/drop)
        podpost: the podpost
        pos: position
        """

        elepos = self._get_ele_pos(podpost)
        if elepos < 0:  # element does not exist in array
            return
        if pos == elepos:  # element is at position
            return

        self.podposts.remove(podpost)
        self.podposts.insert(pos, podpost)

    @sanitize_podpost
    def move_up(self, podpost) -> int:
        """
        move podpost one position up
        """

        ind = self._get_ele_pos(podpost)
        if ind > 0:
            self.podposts.remove(podpost)
            self.podposts.insert(ind - 1, podpost)
        return ind - 1

    @sanitize_podpost
    def move_down(self, podpost):
        """
        move podpost one position down
        """

        ind = self._get_ele_pos(podpost)
        if ind < len(self.podposts):
            self.podposts.remove(podpost)
            self.podposts.insert(ind + 1, podpost)

    def get_podposts(self) -> Iterator[POST_ID_TYPE]:
        """
        Get a list of all podposts
        """

        for podpost in self.podposts:
            yield podpost

    def get_top_id(self) -> Optional[POST_ID_TYPE]:
        if not self._get_top():
            return None
        return self._get_top().id

    def get_podposts_ids(self) -> Iterator[POST_ID_TYPE]:
        yield from self.get_podposts()

    def count(self) -> int:
        return len(self.podposts)

    def play(self):
        """
        start playing
        """

        top = self._get_top()
        if top:
            return top.play()

    def stop(self, position):
        """
        Stop and save position
        """

        podpost = self._get_top()
        if podpost:
            podpost.stop(position)

    def position(self, position):
        """
        set top element's position in seconds
        """

        podpost = self._get_top()
        if podpost:
            podpost.set_position(position)
            podpost.save()

    def pause(self, position):
        """
        pause situation
        """
        podpost = self._get_top()
        if podpost:
            podpost.pause(position)

    def set_duration(self, duration):
        """
        Set duration of element if not set
        """

        top = self._get_top()
        if top:
            top.set_duration(duration)

    def is_in_queue(self, podpost: Podpost) -> bool:
        return podpost.id in self.podposts

    def sanitize(self):
        to_remove: List[int] = []
        for postid in self.podposts:
            podpost = PodpostFactory().get_podpost(postid)
            if not podpost:
                persist_log(LogType.UnknownPodpostId,
                            msg="Podpost %s is in queue but not found. Will be removed." % postid)
                to_remove.append(postid)
        for id in to_remove:
            self.podposts.remove(id)


class QueueFactory(metaclass=Singleton):
    """
    Factory which creates a Queue if it does not exist or gets the pickle
    otherwise it returns the Singleton
    """

    def __init__(self):
        self.queue = Queue(QueueData.get_queue())

    def get_queue(self) -> Queue:
        """
        Get the queue
        """
        return self.queue

    def get_podposts_objects(self) -> Iterator[Podpost]:
        for postid in self.get_queue().podposts:
            podpost = PodpostFactory().get_podpost(postid)
            if podpost:
                yield podpost


    def get_first_podpost_object(self):
        return next(self.get_podposts_objects(), None)
