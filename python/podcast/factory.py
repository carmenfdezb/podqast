"""
factory for podcast. This factory glues all the cache persistant and new element stuff
for all used podcast elements.

Factory is a Singleton
"""

import sys

from podcast.constants import Constants

sys.path.append("../")

from podcast.singleton import Singleton
from podcast.deprecatedstore import DeprecatedStore


class BaseFactory(metaclass=Singleton):
    store: DeprecatedStore

    def __init__(self, progname="harbour-podqast"):
        """
        Initialization
        store: store for feeds
        """

        self.queue = None
        self.object = None



        self.store = DeprecatedStore(Constants().storepath)




class Factory(BaseFactory):
    """
    Factory for
    """

    def __init__(self):
        super().__init__()

    def get_store(self):
        """
        get the store
        """

        return self.store

