"""
podcast classes
"""
from __future__ import annotations
import logging

logging.basicConfig(level=logging.DEBUG)
logging.getLogger("peewee").setLevel(logging.WARNING)
logging.getLogger('asyncio').setLevel(logging.WARNING)
POST_ID_TYPE = int
