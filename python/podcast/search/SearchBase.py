import logging
from abc import abstractmethod, ABC

from peewee import ModelSelect
from podcast.inbox import InboxEntry
from podcast.podpost import Podpost
from podcast.search.Order import Order
from podcast.search.Filter import Filter

logger = logging.getLogger(__name__)


class SearchBase(ABC):

    def __init__(self) -> None:
        self.search_env = {}

    @abstractmethod
    def get_search_base(self) -> ModelSelect:
        raise ValueError("do not call abstract method")

    @abstractmethod
    def unpack_result_to_podpost(self, result) -> Podpost:
        raise ValueError("do not call abstract method")

    # -> Iterator[Podpost]
    def iter_search(self, filter: Filter, order: Order):
        query = self.get_search_base()
        if filter is not None:
            query = filter.apply_filter(query, self.search_env)
        else:
            logger.warning("no filter set")
        if order is not None:
            query = order.apply_order(query, self.search_env)
        else:
            logger.warning("no order set")
        logger.debug(query)
        for result in query.iterator():
            yield self.unpack_result_to_podpost(result)


class AllSearchBase(SearchBase):
    def get_search_base(self) -> ModelSelect:
        return Podpost.select()

    def unpack_result_to_podpost(self, result) -> Podpost:
        return result


class FavoriteSearchBase(SearchBase):
    def get_search_base(self) -> ModelSelect:
        return Podpost.select().where(Podpost.favorite == True)

    def unpack_result_to_podpost(self, result) -> Podpost:
        return result


class InboxSearchBase(SearchBase):
    def __init__(self) -> None:
        super().__init__()
        self.search_env['list_type'] = InboxEntry

    def get_search_base(self) -> ModelSelect:
        return InboxEntry.select(Podpost).join(Podpost)

    def unpack_result_to_podpost(self, result) -> Podpost:
        return result.podpost
