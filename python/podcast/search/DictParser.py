from typing import Dict, Iterator

from podcast.podcast import PodcastFactory
from podcast.search.Filter import AndFilter, PodpostTextFilter, FavoriteFilter, PodpostListenedFilter, Filter, \
    PodcastFilter
from podcast.search.Order import Order, PodpostPublishedDateOrder, Direction, PodpostListInsertDateOrder
from podcast.search.SearchBase import AllSearchBase


def iterator_from_searchoptions(searchoptions: Dict) -> Iterator[Dict]:
    podcast_cache = {}
    if "podcast_url" in searchoptions:
        podcast_cache[searchoptions["podcast_url"]] = PodcastFactory().get_podcast(searchoptions["podcast_url"])

    filter = create_filter_from_options(searchoptions, podcast_cache)
    order = create_order_from_options(searchoptions)
    return (post.get_data() for post in AllSearchBase().iter_search(filter, order))



def create_filter_from_options(searchoptions: Dict, podcast_cache: Dict = None) -> Filter:
    if podcast_cache is None:
        podcast_cache = {}

    def and_add_filter(old, new):
        if old is None:
            return new
        if new is None:
            return old
        return AndFilter(old, new)

    filter = None
    if "searchtext" in searchoptions:
        searchtext = searchoptions["searchtext"]
        filter = and_add_filter(filter, PodpostTextFilter(searchtext))
    if "fav_filter" in searchoptions:
        filter = and_add_filter(filter, FavoriteFilter(searchoptions["fav_filter"]))
    if "listened_filter" in searchoptions:
        filter = and_add_filter(filter, PodpostListenedFilter(searchoptions["listened_filter"]))
    if "podcast_url" in searchoptions:
        url = searchoptions["podcast_url"]
        if url not in podcast_cache:
            raise AttributeError("podcast filter defined but podcast not cached")
        filter: Filter = and_add_filter(filter, PodcastFilter(podcast_cache[url]))
    if "podcast_id" in searchoptions:
        filter: Filter = and_add_filter(filter, PodcastFilter(searchoptions["podcast_id"]))
    return filter


def create_order_from_options(searchoptions: Dict) -> Order:
    dir = Direction.DESC
    if "order_dir" in searchoptions:
        if searchoptions["order_dir"] == "asc":
            dir = Direction.ASC
    order: Order = PodpostPublishedDateOrder(dir)

    if "order" in searchoptions:
        if searchoptions["order"] == "published":
            order = PodpostPublishedDateOrder(dir)
        if searchoptions["order"] == "inserted":
            order = PodpostListInsertDateOrder(dir)
    return order
