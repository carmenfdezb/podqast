"""
List of subscribed podcasts
"""

import sys
from typing import Tuple, Iterator

from podcast.persistent_log import persist_log, LogType

sys.path.append("../")

from podcast.singleton import Singleton
from podcast.podcast import PodcastFactory, Podcast
from podcast import util
from podcast import gpodder_import
import pyotherside
import logging

logger = logging.getLogger(__name__)

listname = "the_podcast_list"


class PodcastList:
    """
    This element holds all podcasts
    """

    def add(self, url):
        """
        Add a podcast

        podcast: the podcast to be added
        """

        Podcast.create_from_url(url)

    def delete_podcast(self, url):
        """
        Remove a podcast from list

        podcast: the podcast to be removed
        """

        PodcastFactory().remove_podcast(url)

    def get_podcasts(self) -> Iterator[str]:
        """
        return the list of subscribed podcasts
        """
        for pc in Podcast.select(Podcast.url).iterator():
            yield pc.url

    def get_podcasts_objects(self, subscribed_only=True) -> Iterator[Podcast]:
        query = Podcast.select()
        if subscribed_only:
            query.where(Podcast.subscribed == True)
        yield from query.order_by(Podcast.title.asc()).iterator()

    def get_podcast_count(self, subscribed_only=True):
        query = Podcast.select()
        if subscribed_only:
            query.where(Podcast.subscribed == True)
        return query.count()

    def refresh(self, moveto, limit=0, full_refresh = False) -> Iterator[Tuple[object, str, str, int]]:
        """
        Refresh all podcasts and put them in queue, index, or archive
        @rtype: Iterator[Tuple[object, str, str, int]], yields the postid, podcastitle, posttitle, move
        """

        pllen = self.get_podcast_count(subscribed_only=True)
        count = 0
        for podcast in self.get_podcasts_objects(subscribed_only=False):
            if podcast:
                try:
                    if podcast.subscribed:
                        for move, post in podcast.refresh(moveto, limit, full_refresh):
                            if not post:
                                logger.error("got a nullpost while refreshing podcast %s",podcast.url)
                            else:
                                yield (
                                post.id,
                                podcast.title,
                                post.title,
                                move
                            )
                except:
                    logger.exception(
                        "podcast refresh failed for %s." % podcast.title
                    )

            count += 1
            pyotherside.send("refreshProgress", count / pllen)

    def save(self):
        """
        Save the PodcastList
        """

        raise NotImplementedError()

    def import_opml(self, opmlfile):
        """
        add podcasts from opml file
        """

        podcounter = 0

        for pod in util.parse_opml(opmlfile):
            logger.info("importing " + pod)
            try:
                self.add(pod)
                podcounter += 1
            except:
                logger.exception("Could not import %s",pod)
                pyotherside.send("apperror", "failed to import " + pod)

        return podcounter

    def import_gpodder(self):
        """
        import podcasts from gpodder database
        TODO: Import the posts
        """

        podcounter = 0

        for pod in gpodder_import.get():
            try:
                self.add(pod)
                podcounter += 1
            except Exception as e:
                logger.exception("Could not import %s",pod)
                pyotherside.send("apperror", "failed to import " + pod)
                persist_log(LogType.AddPodcastError, podcast=pod, exception=e)

        return podcounter


class PodcastListFactory(metaclass=Singleton):
    """
    The Factory of fhe PodcastList
    """



    def get_podcast_list(self):
        """
        Get the podcast list
        """

        return PodcastList()
